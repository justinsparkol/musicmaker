package bus {
	
	
	/**
	 * The Sparkol Message sends messages around the system, without using event listeners which are too heavy
	 * 
	 * @author Jon air
	 */
	public class SparkolBus {
		
		private static var listeners:Vector.<SparkolBusListener>;
		private static var lastUsedUniqueID:uint;
		
		public static function init():void {
			// expected to only be called once, but could also clear down listeners & messages!?
			listeners = new Vector.<SparkolBusListener>();
			lastUsedUniqueID = 1000; // just so it's not just from zero!?
		}
		
		private static function findFirstMessage(messageID:String, funcIfKnown:Function = null):int { // returns index
			for (var i:int = 0; i < listeners.length; i++) {
				if ((listeners[i].messageID == messageID) && ((funcIfKnown == null) || (listeners[i].toExecute == funcIfKnown))) return i;
			} // for
			return -1;
		}
		
		public static function tell(messageID:String, action:String = '', data:Object = null):void {
			
			// pass on message to all interested parties
			for (var i:int = 0; i < listeners.length; i++) {
				if ((listeners[i].messageID == messageID) && ((listeners[i].action == action) || (listeners[i].action ==''))) {
					//trace('Telling system about ' + listeners[i].messageID + ', calling ' + String(listeners[i].toExecute));
					if (listeners[i].toExecute != null) listeners[i].toExecute(data);
				}
			} // for
		}
		
		public static function listen(messageID:String, func:Function, action:String = ''):void {
			// TODO: check for duplicates?  
			//trace('adding function ' + func + ' for ' + messageID);
			listeners.push(new SparkolBusListener(messageID, action, func));
		}
		
		public static function forget(messageID:String, func:Function = null):void {
			var indx:int = -1;
			while (indx = findFirstMessage(messageID, func)) { // this gives warning, but is correct
				if (indx < 0) break;
				listeners.splice(indx, 1);
			} // while
		}
		
		public static function generateUniqueString(optionalIdentifier:String):String {
			lastUsedUniqueID++;
			return optionalIdentifier + String(lastUsedUniqueID);
		}
		
	}

}