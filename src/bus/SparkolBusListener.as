package bus 
{
	/**
	 * Used by SparkolBus as data type for listeners 
	 * @author Jon air
	 */
	public class SparkolBusListener 
	{
		public var messageID:String; // SparkolBusMessage
		public var action:String; // SparkolBusAction
		public var toExecute:Function; // Function to run if message happens
		
		public function SparkolBusListener(messageID:String, action:String, toExecute:Function):void { 
			this.messageID = messageID;
			this.action = action;
			this.toExecute = toExecute;
		}
		
	}

}