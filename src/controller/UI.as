package controller {
	import bus.SparkolBus;
	//import com.greensock.easing.Expo;
	//import com.greensock.TweenLite;
	import flash.display.MovieClip;
	import model.Sequencer;
	//import model.Utils;
	//import view.Slider;	
	
	import flash.events.Event;

	
	/**
	 ** @author Justin O'Neill
	 *  functionality for controlling the interface elements
	 **/
	
	public class UI extends MovieClip {
		
		private var userInterface:UI;
		protected var sequencer:Sequencer;
		protected var songLength:Number;
		protected var selectedFolder:String;
		
		//constructor; initialise tint plugin, hide the UI and wait to be added to the stage
		public function UI(sequencer:Sequencer, songLength:Number):void {
			this.sequencer = sequencer;
			this.songLength = songLength;
			SparkolBus.listen(Sequencer.AUDIO_LOADED, audioLoaded);
		}
		
		//passes the interface slider values to the sequencer and initiates the composer
		protected function handleGo(e:Event = null):void {
			sequencer.initComposer(songLength, selectedFolder);
		}
		
		//stop the playback
		protected function handleStop(e:Event = null):void {
			sequencer.stopSequence();
		}
		
		//update the interface and begin the sequence
		protected function audioLoaded(e:Event = null):void {
			SparkolBus.forget(Sequencer.AUDIO_LOADED, audioLoaded);
			sequencer.playSequence();
		}
		
	}
	
}