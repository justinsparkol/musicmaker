package  {
	
	import bus.SparkolBus;
	import flash.display.Sprite;
	import model.Sequencer;
	import view.Sliders.SliderUI;
	
	/**
	 ** @author Justin O'Neill
	 *  provides an interface for users to choose a song based on three sliders, which loads layers of mp3 loops and sequences them
	 *  based on song length, "song design" rules, fuzzy logic and randomisation. The song is then played through a buffer, loop by loop.
	 *  Each time a song is created there are differences to the mix and structure.
	 **/
	
	[SWF(width='1000', height='600', backgroundColor='0xFFFFFF', frameRate="30" )]
	public class Main extends Sprite {
		
		private var userInterface:SliderUI;
		private var sequencer:Sequencer;
		private var songLength:Number // in seconds

		public function Main(targetLength:int = 180) {
			
			songLength = targetLength;
			
			// setup the bus
			SparkolBus.init();

			sequencer = new Sequencer();
			userInterface = new SliderUI(sequencer, songLength);
			addChild(userInterface);
			
		}
		
	}
	
}