package view.Audition {
	import bus.SparkolBus;
	import controller.UI;
	import flash.filesystem.File;
	import model.Sequencer;
	import model.SoundManager;
	import model.Utils;
	import com.greensock.plugins.TintPlugin;
	import com.greensock.plugins.TweenPlugin;
	import com.greensock.easing.Expo;
	import com.greensock.TweenLite;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	/**
	 ** @author Justin O'Neill
	 *  brings all visual elements to the screen and sets up mouse events 
	 *  and functionality for controlling the interface elements
	 **/
	
	public class AuditionUI extends UI {
		
		public static const GO:String = 'go';
		public static const STOP:String = 'stop';
		private var goButton:GoButton;
		private var dialogBase:DialogBase;
		private var f:File;
		
		//constructor; initialise tint plugin, hide the UI and wait to be added to the stage
		public function AuditionUI(sequencer:Sequencer, songLength:Number):void {
			super(sequencer, songLength);
			TweenPlugin.activate([TintPlugin]);
			addEventListener(Event.ADDED_TO_STAGE, init, false, 0, true);
		}
	
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);		
			TweenLite.to(this, 0.01, { tint:0x000000, onComplete:addScreen } );
		}
		
		//add interface to screen and set up mouse down events for all sliders, and then fade it in
		private function addScreen():void {
			dialogBase = new DialogBase();
			
			goButton = new GoButton();
			goButton.addEventListener(MouseEvent.CLICK, handleGo);
			goButton.x = 142;
			goButton.y = 197;
			goButton.buttonText.mouseEnabled = false;
			goButton.useHandCursor = true;
			goButton.buttonMode = true;
			
			addChild(dialogBase);
			addChild(goButton);
			fadeIn(0.5);
			SparkolBus.listen(SoundManager.CLIPPING, clipping);
		
		}
		
		private function clipping(e:Event):void {
			//trace('clipping');
		}
		
		public function fadeIn(fadeDelay:Number):void {
			TweenLite.to(this, 1, { tint:null, delay:fadeDelay } );
		}
		
		//update the interface to disable go button during processing; dispatch a GO event
		override protected function handleGo(e:Event = null):void {
			goButton.useHandCursor = false;
			goButton.buttonMode = false;
			goButton.removeEventListener(MouseEvent.CLICK, handleGo);
			TweenLite.to(goButton.background, 0.01, { tint:0xcccccc } );
			f = new File;
			f.addEventListener(Event.SELECT, onFolderSelected);
			f.browseForDirectory("Choose a directory");
		}
		
		private function onFolderSelected(e:Event = null):void {
			selectedFolder = f.nativePath;
			dialogBase.monster.gotoAndPlay('munch');
			
			super.handleGo(null);
		}
		
		//update the interface to display a go button; dispatch a STOP event
		override protected function handleStop(e:Event = null):void {
			goButton.buttonText.text = 'Go!';
			goButton.removeEventListener(MouseEvent.CLICK, handleStop);
			goButton.addEventListener(MouseEvent.CLICK, handleGo);
			super.handleStop(e);
		}

		//update the interface to enable a stop button once processing has finished and the music is playing
		override protected function audioLoaded(e:Event = null):void {
			finishedProcessing();
			super.audioLoaded(null);
		}
		
		private function finishedProcessing():void {
			dialogBase.monster.gotoAndStop(1);
			goButton.useHandCursor = true;
			goButton.buttonMode = true;
			TweenLite.to(goButton.background, 0.01, { tint:null } );
			goButton.buttonText.text = 'Stop';
			goButton.addEventListener(MouseEvent.CLICK, handleStop);
		}
			
	}
	
}