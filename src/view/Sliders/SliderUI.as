package view.Sliders {
	import controller.UI;
	import com.greensock.plugins.TintPlugin;
	import com.greensock.plugins.TweenPlugin;
	import com.greensock.TweenLite;
	import com.greensock.easing.Expo;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import model.Sequencer;
	import model.Utils;
	
	/**
	 ** @author Justin O'Neill
	 **/
	
	public class SliderUI extends UI {

		private const SLIDER_WIDTH:Number = 622;
		public var controlHappiness:Slider;
		public var controlStyle:Slider;
		public var controlSpeed:Slider;
		
		private var controlledSlider:Slider = null;
		private var goButton:GoButton;
		private var dialogBase:DialogBase;
	
		public function SliderUI(sequencer:Sequencer, songLength:Number):void {
			super(sequencer, songLength);
			TweenPlugin.activate([TintPlugin]);
			addEventListener(Event.ADDED_TO_STAGE, init, false, 0, true);
		}
		
		//add mouse up event to stage;
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.addEventListener(MouseEvent.MOUSE_UP, endSlide);
		
			TweenLite.to(this, 0.01, { tint:0xffffff, onComplete:addScreen } );
		}

		//add interface to screen and set up mouse down events for all sliders, and then fade it in
		private function addScreen():void {
			
			dialogBase = new DialogBase();
			
			controlHappiness = new Slider();
			controlHappiness.x = 182.6;
			controlHappiness.y = 188.05;
			controlHappiness.controlSlider.sliderButton.addEventListener(MouseEvent.MOUSE_DOWN, handleSliderDown);
			
			controlStyle = new Slider();
			controlStyle.x = 182.6;
			controlStyle.y = 280.8;
			controlStyle.controlSlider.sliderButton.addEventListener(MouseEvent.MOUSE_DOWN, handleSliderDown);
			
			controlSpeed = new Slider();
			controlSpeed.x = 182.6;
			controlSpeed.y = 372.9;
			controlSpeed.controlSlider.sliderButton.addEventListener(MouseEvent.MOUSE_DOWN, handleSliderDown);
					
			goButton = new GoButton();
			goButton.addEventListener(MouseEvent.CLICK, handleGo);
			goButton.x = 438.3;
			goButton.y = 481.05;
			goButton.buttonText.mouseEnabled = false;
			goButton.useHandCursor = true;
			goButton.buttonMode = true;
			
			addChild(dialogBase);
			addChild(controlHappiness);
			addChild(controlStyle);
			addChild(controlSpeed);
			addChild(goButton);
			
			fadeIn(0.5);
		}
		
		public function fadeIn(fadeDelay:Number):void {
			TweenLite.to(this, 1, { tint:null, delay:fadeDelay } );
		}

		
		//update the interface to disable go button during processing; dispatch a GO event
		override protected function handleGo(e:Event = null):void {
			
			goButton.useHandCursor = false;
			goButton.buttonMode = false;
			goButton.removeEventListener(MouseEvent.CLICK, handleGo);
			TweenLite.to(goButton.background, 0.01, { tint:0xcccccc } );
			
			dialogBase.monster.gotoAndPlay('munch');
			
			//userInterface.controlHappiness.theValue, userInterface.controlStyle.theValue, userInterface.controlSpeed.theValue, songLength
			super.handleGo(e);
		}
		
		//update the interface to display a go button; dispatch a STOP event
		override protected function handleStop(e:Event = null):void {
			
			goButton.buttonText.text = 'Go!';
			goButton.removeEventListener(MouseEvent.CLICK, handleStop);
			goButton.addEventListener(MouseEvent.CLICK, handleGo);
			
			super.handleStop(e);
		}
		
		//begin the slide on a particular slider
		private function handleSliderDown(e:Event):void {
			if(controlledSlider == null) controlledSlider = e.currentTarget.parent.parent as Slider;
			stage.addEventListener(MouseEvent.MOUSE_MOVE, moveSlider);
			moveSlider(e);
		}
		
		//update the selected slider to reflect the mouse position
		private function moveSlider(e:Event = null):void {
			var wid:Number = (controlledSlider.controlSlider.sliderButton.width / 2);
			var position:Number = Utils.limitRange(controlledSlider.mouseX, wid, SLIDER_WIDTH);
			controlledSlider.theValue = Utils.getPercent(position, wid, SLIDER_WIDTH);
			TweenLite.to(controlledSlider.controlSlider.sliderButton, 0.2, { x:position - wid, ease:Expo.easeOut, overwrite:1 } );
			TweenLite.to(controlledSlider.controlSlider.sliderShadow, 0.2, { x:position - wid, ease:Expo.easeOut, overwrite:1 } );
		}
		
		//release the slider and remove the stage mouse move event
		private function endSlide(e:Event = null):void {
			if (controlledSlider != null) {
				stage.removeEventListener(MouseEvent.MOUSE_MOVE, moveSlider);
				controlledSlider = null;
			}
			
		}
		
		override protected function audioLoaded(e:Event = null):void {
			finishedProcessing();
			super.audioLoaded(null);
		}
		
		private function finishedProcessing():void {
			dialogBase.monster.gotoAndStop(1);
			goButton.useHandCursor = true;
			goButton.buttonMode = true;
			TweenLite.to(goButton.background, 0.01, { tint:null } );
			goButton.buttonText.text = 'Stop';
			goButton.addEventListener(MouseEvent.CLICK, handleStop);
		}
		
	}
	
}