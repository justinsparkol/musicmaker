package view.Sliders {
	import flash.display.MovieClip;
	
	/**
	 ** @author Justin O'Neill
	 *  visual interface element provides the user with the ability to select 
	 *  a value between a minimum and maximum value
	 **/
	
	public class Slider extends MovieClip {
		
		public var controlSlider:ControlSlider;
		private var _theValue:int;
		
		//initialise the controlslider asset 
		public function Slider():void {
			controlSlider = new ControlSlider();
			controlSlider.sliderButton.buttonMode = true;
			controlSlider.sliderButton.useHandCursor = true;
			addChild(controlSlider);
		}

		//sets the value of the slider position
		public function set theValue(value:int):void {
			_theValue = value;
		}
		
		//gets the value of the slider position
		public function get theValue():int {
			return _theValue;
		}
	}
	
}