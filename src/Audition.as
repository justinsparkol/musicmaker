package {
	
	import bus.SparkolBus;
	import flash.display.Sprite;
	import view.Audition.AuditionUI;
	import model.Sequencer;
	
	/**
	 ** @author Justin O'Neill
	 *  provides an interface for users to choose a FOLDER to load a skeleton song, which loads layers of mp3 loops and sequences them
	 *  based on song length, "song design" rules, fuzzy logic and randomisation. The song is then played through a buffer, loop by loop.
	 *  Each time a song is created there are differences to the mix and structure.
	 **/
	
	[SWF(width='400', height='300', backgroundColor='0x000000', frameRate="30" )]
	public class Audition extends Sprite {
		
		private var userInterface:AuditionUI;
		private var sequencer:Sequencer;
		private var songLength:Number // in seconds
		
		public function Audition(targetLength:int = 120) {
			
			songLength = targetLength;
			
			// setup the bus
			SparkolBus.init();
			
			sequencer = new Sequencer();
			userInterface = new AuditionUI(sequencer, songLength);
			addChild(userInterface);
			
		}

	}
	
}