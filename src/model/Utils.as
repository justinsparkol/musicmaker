package model {
	
	/**
	 ** @author Justin O'Neill
	 *  static utility class containing numerical and data manipulation functions required for various classes
	 **/
	
	public class Utils {
		
		//returns a number limited within a certain range
		public static function limitRange( X:Number, minValue:Number, maxValue:Number ):Number {
			X = X < minValue ? minValue : X;
			X = X > maxValue ? maxValue : X;
			return X;
		}
		
		//returns an int as a percentage of a certain range
		public static function getPercent( X:Number, minValue:Number, maxValue:Number):int {
			return Math.round((X - minValue)/(maxValue - minValue) * 100);
		}
		
		//sorts a vector (vec) by object property (str)
		public static function sortOn(str:String, vec:Vector.<Object>):Vector.<Object> {
			var array:Array = [];
			while (vec.length > 0) array.push(vec.pop());

			array.sortOn(str, Array.NUMERIC);
			while (array.length > 0) vec.push(array.pop());
			return vec;
			
			/* //can't get this vector sorting function working..
			 * 
			var sortingFunction:Function = function(itemA:Object, itemB:Object):Number {
				if (itemA.slider1.valueOf() < itemB.slider1.valueOf()) return -1; //ITEM A is before ITEM B
				else if (itemA.slider1.valueOf() > itemB.slider1.valueOf()) return 1; //ITEM A is after ITEM B
				else return 0; //ITEM A and ITEM B are the same
			}
			vec.sort(sortingFunction);
			*/
		}
		
		//returns a random element from a vector
		public static function getRandVectorElement(vec:Vector.<String>):Vector.<String> {
			var num:int = Math.floor(Math.random() * vec.length);
			return Vector.<String>(vec[num].split('|'));
		}
		
		//returns yes/no with an 60% chance of yes.
		public static function randBool():Boolean {
			if (Math.random() * 1 < 0.6) {
				return true;
			} else {
				return false;
			}
		}
		//normalise float to within range
		public static function checkValidFloat(val:Number):Number {
			if (val < -1) {
				val = -1;
			} else if (val > 1) {
				val = 1;
			}
			return val;
		}

	}
}