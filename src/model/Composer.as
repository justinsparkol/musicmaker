﻿package model {
	import bus.SparkolBus;
	import model.data.Skeletons;
	import model.SongScore;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
		
	/**
	 ** @author Justin O'Neill
	 *  Composer takes in information provided through the interface and the song length,
	 * 	chooses an appropriate skeleton file based on best fit analysis
	 **/
	
	public class Composer extends EventDispatcher {
		
		public static const TRACK_COMPOSED:String = 'track_composed';
		
		private var skeletons:Skeletons;
		
		private var data:XML;
		public var layerData:Vector.<Vector.<AudioObject>>;
		public var numOfFilesToLoad:int;
		private var skeletonFolder:String;
		public var songScore:SongScore;
		private var songLength:Number;
		private var numPatterns:int;
		
		private var beatspermin:int;
		private var beatspermeasure:int;
		private var numofmeasures:int;
		public var loopLength:Number;
		
		private var mood:int;
		private var style:int;
		private var speed:int;
		
		//constructor
		public function Composer(songLength:Number) {
			this.songLength = songLength;
		}
		
		public function loadMusicSkeleton(folder:String):void {
			numOfFilesToLoad = 0;
			if (folder == null) {
				skeletonFolder = chooseSkeleton();
			} else {
				skeletonFolder = folder;
			}
			loadSkeleton(skeletonFolder + '/skeleton.xml');
		}
		
		//TODO: fuzzy/best fit logic to choose a skeleton based on the slider values
		//and values in skeletonsxml - passes back a folder string containing the skeleton
		private function chooseSkeleton():String {
			skeletons = new Skeletons();
			skeletonFolder = 'music/03 10 2014 F minor Soul 95bpm';
			return skeletonFolder;
		}
		
		//load selected skeleton file
		private function loadSkeleton(skeletonPath :String):void {
			var ldr:URLLoader = new URLLoader();
			ldr.addEventListener(Event.COMPLETE, loadComplete);
			ldr.addEventListener(IOErrorEvent.IO_ERROR, loadError);
			ldr.load(new URLRequest(skeletonPath));
		}
		
		//handle errors loading skeleton file
		private function loadError(e:IOErrorEvent = null):void {
			trace('error loading skeleton:' + e.text);
		}
		
		//handle loading complete event and call data parse
		private function loadComplete(e:Event):void {
			parseData(new XML(e.target.data));
		}
		
		//parse xml data into layer vector of vectors containing audio objects
		//which reference a volume and an mp3 file url, including the chosen skeleton folder
		private function parseData(xml:XML):void {
			
			layerData = new Vector.<Vector.<AudioObject>>();
			
			var layerLength:int = xml.layer.length();
			var partNumber:int;
			var introParts:Vector.<int> = new Vector.<int>;
			
			beatspermin = xml.@bpminute;
			beatspermeasure = xml.@bpmeasure;
			numofmeasures = xml.@numMeasures;
			numPatterns = xml.@numPatterns;
			
			var BPSecond:Number = beatspermin / 60;
			var totalBeats:int = beatspermeasure * numofmeasures;
			loopLength = totalBeats / BPSecond;
			
			var tracks:Vector.<AudioObject>;
			var okForIntro:Boolean;
			for (var a: int = 0; a < layerLength; a++) {
				partNumber = xml.layer[a].audio.length();
				okForIntro = xml.layer[a].@okForIntro == '1' ? true : false;
				tracks = new Vector.<AudioObject>();
				for (var b: int = 0; b < partNumber; b++) {
					var audioObj:AudioObject = new AudioObject();
					audioObj.mp3Vol = xml.layer[a].audio[b].volume[0].toString();
					audioObj.mp3File = skeletonFolder + '\\' + xml.layer[a].audio[b].mp3file[0].toString();
					
					tracks.push(audioObj);
					
					numOfFilesToLoad++;
				}
				
				introParts.push(okForIntro);
				layerData.push(tracks);
			}
			composeTrack(introParts);
		}
		
		//compose a track and dispatch an event to the sequencer to begin loading the sounds
		private function composeTrack(introParts:Vector.<int>):void {
			songScore = new SongScore(songLength, loopLength, numPatterns, introParts);
			SparkolBus.tell(TRACK_COMPOSED);
		}
		
		//returns the relevant audio object mp3 reference for the pattern chosen in songscore
		public function getFileForSequence(layer:int, pattern:int):String {
			if (pattern == 0) return '';
			
			var mp3File:String;
			if (layer < layerData.length && ((pattern - 1) < layerData[layer].length)) {
				//trace('pattern = ', layer, pattern);
				return layerData[layer][pattern - 1].mp3File;
			}
			return '';
		}
		
	}
	
}
