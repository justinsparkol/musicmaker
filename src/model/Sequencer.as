package model {
	import bus.SparkolBus;
	import com.greensock.TweenLite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.media.Sound;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	import model.Composer;
	import model.data.MP3Loader;
	
	/**
	 ** @author Justin O'Neill
	 *  Sequencer manages the composer class and the SoundManager class and loads sound
	 *  and plays them in sequence
	 **/
	
	public class Sequencer extends EventDispatcher {
		
		public static const AUDIO_LOADED:String = 'audio_loaded';
		public static const AUDIO_ENDED:String = 'audio_ended';
		
		private var soundMGR:SoundManager;
		private var composer:Composer;
		private var vecFiles:Vector.<String>;
		private var vecVols:Vector.<Number>;
		private var mp3Loader:MP3Loader;
		
		//main contructor sets up a new SoundManager class to handle the data
		public function Sequencer()	{
			soundMGR = new SoundManager();
		}
		
		//initialise the Composer class
		//composer listens for TRACK_COMPOSED event to begin loading sounds
		public function initComposer(songLength:Number, selectedFolder:String = ''):void {
			//soundsLoaded = 0;
			composer = new Composer(songLength);
			composer.loadMusicSkeleton(selectedFolder);
			soundMGR.initSoundManager();
			SparkolBus.listen(Composer.TRACK_COMPOSED, loadSounds);
		}
		
		public function loadSounds(e:Event = null):void {
			SparkolBus.forget(Composer.TRACK_COMPOSED, loadSounds);
			vecFiles = new Vector.<String>;
			vecVols = new Vector.<Number>;
			var partsVector:Vector.<AudioObject>;
			
			for (var a:int = 0; a < composer.layerData.length; a++) {
				partsVector = composer.layerData[a];
				
				for (var b:int = 0; b < partsVector.length; b++) {
					var audioObj:AudioObject = partsVector[b];
					vecFiles.push(audioObj.mp3File);
					vecVols.push(audioObj.mp3Vol);
				}
			}
	
			SparkolBus.listen(MP3Loader.MP3_LOADED, registerSound);
			SparkolBus.listen(MP3Loader.ALL_LOADED, allSoundsLoaded);
			mp3Loader = new MP3Loader();
			mp3Loader.loadSounds(vecFiles);
			
		}

		private function registerSound(e:Event):void {
			soundMGR.registerSound(vecFiles[mp3Loader.soundsLoaded], e.currentTarget as Sound, composer.loopLength, vecVols[mp3Loader.soundsLoaded] * 0.01);
		}
		
		private function allSoundsLoaded(e:Event = null):void {
			SparkolBus.forget(MP3Loader.MP3_LOADED, registerSound);
			SparkolBus.forget(MP3Loader.ALL_LOADED, allSoundsLoaded);
			SparkolBus.tell(AUDIO_LOADED);
		}
		
		//begins the stream in sound manager and initialise the recursive playLoop method below
		public function playSequence():void {
			
			soundMGR.trackVolume = 0;
			soundMGR.beginStream();
			playNextSequence(0);
			soundMGR.adjustTrackVolume(1, int(Utils.randBool())*composer.loopLength);
			
		}
		
		
		//goes through each layer in loopNumber in composer.songScore.songStructure
		//and adds mp3 urls to a playsequence list, which is then passed into the soundmanager
		private function playNextSequence(sequenceNumber:int):void {
			
			
			
			if (sequenceNumber < composer.songScore.songStructure.length) {
				
				trace('Sequence Number', sequenceNumber + 1);
				
				var layerVector:Vector.<Vector.<String>> = composer.songScore.songStructure[sequenceNumber];
				var numPart:String;
				var measureParts:Vector.<String> = new Vector.<String>;
				var stringPart:String;
				sequenceNumber++;
				
				var i:int;

				for (var n:int = 0; n < layerVector.length; n++) {
					if (layerVector[n] != null) {
						measureParts = new Vector.<String>;
						for (i = 0; i < layerVector[n].length; i++) {
							numPart = layerVector[n][i];
							if (numPart != '') {
								measureParts.push( composer.getFileForSequence(n, int(numPart)) );
							} else {
								measureParts.push('');
							}
						}
						if (measureParts != null) {
							
							//fade out last loop -  
							if (sequenceNumber == composer.songScore.songStructure.length - 1) {
								 if (composer.songScore.outroNumSequences > 0) {
									 soundMGR.adjustTrackVolume(0, composer.loopLength);
								 } else {
									 soundMGR.adjustTrackVolume(0, composer.loopLength, composer.loopLength * composer.songScore.patternLength);
								 }
								
								
							}
							
							if (n == 2) { //hardcoded 'main' layer .. urg
								soundMGR.playSequence(measureParts, playNextSequence, [sequenceNumber]);
							} else {
								soundMGR.playSequence(measureParts);
							}
							
						}
					}
				}
				
				
				

			} else {
				
				trace('End Sequence');
				endPlay();
			}
		}
		
		//when all loops have been played, stop the stream
		//and write to mp3 if needed
		private function endPlay():void {
			stopSequence();
			SparkolBus.tell(AUDIO_ENDED);
			
			CONFIG::saveToMP3 {
				TweenLite.delayedCall(1, soundMGR.exportSongToMP3);
			}
		}
		
		//handle stop mid loop
		public function stopSequence():void {
			soundMGR.endStream();
		}
	}
}