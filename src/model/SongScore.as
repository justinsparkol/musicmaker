package model {
	import model.Utils;
	/**
	 ** @author Justin O'Neill
	 *  SongScore creates the full structure of the song in the form of a vector (loops) of vectors (layers) of vectors (audio objects)
	 *  Each vector of audio objects contains two alternative parts, and is used, in conjunction with some randomisation and hard coded
	 *  'song structure formula' rules 
	 **/
	
	public class SongScore {
		
		public var songStructure:Vector.<Vector.<Vector.<String>>>;  //TODO:clarity here.. split out each vector to a custom class
		
		private var introNumSequences:int;
		private var middleNumSequences:int; 
		public var outroNumSequences:int;//make available to sequencer in case there are no outro sequences for fade out
		
		private const numberOfLayers:int = 8;
		private const introStructures:Vector.<String> = Vector.<String>(['1']);
		private const twoPatternStructures:Vector.<String> = Vector.<String>(['1|1|2|1', '1|2|1|2', '1|2|2|1', '1|2|1|2', '1|1|2|2']);
		private const fourPatternStructures:Vector.<String> = Vector.<String>(['1|2|3|4', '1|3|2|4', '1|2|4|3', '1|4|2|3', '1|2|1|3', '2|3|2|4', '1|2|1|3']);
		private const shortTwoPatternStructures:Vector.<String> = Vector.<String>(['1|2', '2|1', '1|1', '2|2']);
		private const shortFourPatternStructures:Vector.<String> = Vector.<String>(['1|2', '2|1', '1|1', '2|2', '1|3', '3|1', '2|3', '1|4', '4|1', '2|4' ]);
		
		
		private var patternStructures:Vector.<String>;
		private var chordPattern:Vector.<String>;
		private var bassPattern:Vector.<String>;
		private var layer3Pattern:Vector.<String>;
		private var layer4Pattern:Vector.<String>;
		public var patternLength:int;

		//iterate through the bars required for this song length, and populate with just the pattern for each part
		public function SongScore(trackLengthInSecs:Number, loopLengthInSecs:Number, numPatterns:int, introParts:Vector.<int>):void {

			if (trackLengthInSecs < 65) {
				patternStructures = (numPatterns == 2) ? shortTwoPatternStructures : shortFourPatternStructures;
			} else {
				patternStructures = (numPatterns == 2) ? twoPatternStructures : fourPatternStructures;
			}
			
			var str:String = patternStructures[0].toString();
			patternLength = str.split('|').length;

			var totalNumSequences:int = setSequences(trackLengthInSecs, loopLengthInSecs);
			songStructure = new Vector.<Vector.<Vector.<String>>>(totalNumSequences);
			
			var layerVec:Vector.<Vector.<String>>;
			var groupPattern:Vector.<String>;
			var numIntroPartsSoFar:int = 0;
			var layerInt:int;
			
			
			
			for (var i:int = 0; i < totalNumSequences; i++) {
				layerVec = new Vector.<Vector.<String>>(numberOfLayers);
				songStructure[i] = layerVec;
				if (i < introNumSequences) { //intro
					
					groupPattern = Utils.getRandVectorElement(introStructures);
					songStructure[i][2] = groupPattern; //main riff
					
					//decide whether to put these parts in, max 3
					for (layerInt = 0; layerInt < numberOfLayers; layerInt++) {
						if (introParts[layerInt] && Utils.randBool() && numIntroPartsSoFar < 3) {
							songStructure[i][layerInt] = groupPattern;
							numIntroPartsSoFar++;
						}
					}
					
				} else if (i >= introNumSequences && i < (introNumSequences + (middleNumSequences))) { //middle
					
					groupPattern = Utils.getRandVectorElement(patternStructures);
					songStructure[i][0] = groupPattern;
					songStructure[i][1] = groupPattern;
					songStructure[i][2] = groupPattern;
					if (Utils.randBool()) songStructure[i][3] = groupPattern;
					if (i > middleOfMiddle()) {
						songStructure[i][4] = groupPattern;
						if (Utils.randBool()) songStructure[i][5] = groupPattern;
						if (Utils.randBool()) songStructure[i][6] = groupPattern;
					}
					if (Utils.randBool()) songStructure[i][7] = groupPattern;
					
				} else if (i >= (introNumSequences + (middleNumSequences)) && i < (introNumSequences + (middleNumSequences) + outroNumSequences)) { //outro
					
					groupPattern = Utils.getRandVectorElement(introStructures);
					songStructure[i][2] = groupPattern;
	
					//decide whether to put these parts in
					for (layerInt = 4; layerInt < numberOfLayers; layerInt++) {
						if (Utils.randBool()) songStructure[i][layerInt] = groupPattern;
					}
				}
			}
		}
		
		
		private function setSequences(trackLengthInSecs:Number, loopLengthInSecs:Number):int {
			
			var trueBars:Number = trackLengthInSecs / loopLengthInSecs;
			var ceilBars:int = Math.ceil(trueBars);
			var floorBars:int = Math.floor(trueBars);
			
			var diffOver:Number = (trackLengthInSecs - (ceilBars * loopLengthInSecs));
			var diffUnder:Number = (trackLengthInSecs - (floorBars * loopLengthInSecs));
			
			diffOver = diffOver < 0 ? -diffOver : diffOver; 
			diffUnder = diffUnder < 0 ? -diffUnder : diffUnder;
			
			var songLengthInBars:int;
			//find closest fit
			songLengthInBars = ceilBars;
			if (diffOver != diffUnder) {
				songLengthInBars = diffOver < diffUnder ? ceilBars : floorBars;
			}

	
			//create a start, middle and end

			introNumSequences = 2; //one pattern per sequence
			if (songLengthInBars < 8) introNumSequences = 0;
			
			middleNumSequences = Math.ceil((songLengthInBars * 0.6) / patternLength);
			
			outroNumSequences = songLengthInBars - (middleNumSequences * patternLength) - introNumSequences; //whatever's left to fill the length
			if (outroNumSequences < 0) outroNumSequences = 0;
			
			return  introNumSequences + middleNumSequences + outroNumSequences;
		}
		
		//return a reference to the middle of the middle (halfway through the impact section)
		private function middleOfMiddle():int {
			return (introNumSequences + ((middleNumSequences / patternLength) / 2));
		}
		
	}
	
}