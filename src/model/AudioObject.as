package model {
	
	/**
	 ** @author Justin O'Neill
	 **/
	
	public class AudioObject extends Object {
		
		private var _mp3Vol:Number;
		private var _mp3File:String;
		
		public function AudioObject():void {}
		
		public function get mp3Vol():Number {
			return _mp3Vol;
		}
		
		public function set mp3Vol(val:Number):void {
			_mp3Vol = val;
		}
		
		public function get mp3File():String {
			return _mp3File;
		}
		
		public function set mp3File(val:String):void {
			_mp3File = val;
		}	
		
	}
	
}