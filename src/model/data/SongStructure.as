package model.data {
	
	/**
	 ** @author Justin O'Neill
	 **/
	
	public class SongStructure {
		
		private var _songStructure:Vector.<Vector.<Vector.<String>>>;
		
		public function SongStructure(totalNumLoops) {
			songStructure = new Vector.<Vector.<Vector.<String>>>(totalNumLoops);
		}
		
		public function set songStructure(val) {
			_songStructure = val;
		}
		
		public function get songStructure(val) {
			return _songStructure;
		}
		
	}
	
}