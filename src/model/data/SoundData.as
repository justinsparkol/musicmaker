package model.data {
	
	import flash.media.Sound;
	
	/**
	 ** @author Justin O'Neill
	 *  class to store data associated with each loop loaded from skeleton files
	 **/
	
	public class SoundData {
		
		public var id:String;
		public var leftData:Vector.<Number>;
		public var rightData:Vector.<Number>;
		public var sound:Sound;
		public var position:int = 0;
		public var length:int;
		public var completionCallback:Function = null
		public var callbackArgs:Array;
		
		public function SoundData():void { }
		
		// returns a clone of the sound data object
		public function dataClone():SoundData {
			var result:SoundData = new SoundData();
			result.id = id;
			result.leftData = leftData;
			result.rightData = rightData;
			result.length = length;
			result.completionCallback = completionCallback;
			result.callbackArgs = callbackArgs;
			result.sound = sound;
			return result;
		}
	}
}