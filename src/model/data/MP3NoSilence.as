package model.data {
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.media.Sound;
	import flash.utils.ByteArray;
	
	/**
	 ** @author Justin O'Neill
	 *  removes the silence from an mp3 based on length and a magic number!
	 *  TODO: save file locally and stream to buffer during playback rather than holding in memory
	 **/
	
	public class MP3NoSilence {
		private const MAGIC_DELAY:Number = 2257.0;
		public var mp3Bytes:ByteArray;
		
		//extracts and stores the loopable part of an mp3
		public function MP3NoSilence(sound:Sound, loopLength:Number):void {
			mp3Bytes = new ByteArray();
			sound.extract(mp3Bytes, loopLength*44100, MAGIC_DELAY);
			mp3Bytes.position = 0;
			
		}

	}
	
}