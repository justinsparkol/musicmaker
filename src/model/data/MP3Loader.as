package model.data {
	import bus.SparkolBus;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.media.Sound;
	import flash.net.URLRequest;
	
	/**
	 ** @author Justin O'Neill
	 **/
	
	public class MP3Loader {
		
		public static const MP3_LOADED:String = 'mp3_loaded';
		public static const ALL_LOADED:String = 'all_loaded';
		
		private var vecFiles:Vector.<String>;
		public var soundsLoaded:int;
		
		public function MP3Loader():void {
			
		}
		
		public function loadSounds(vecFiles:Vector.<String>):void {
			this.vecFiles = vecFiles;
			soundsLoaded = 0;
			loadSound(vecFiles[soundsLoaded]);
		}
		
		//load a sound
		private function loadSound(mp3FilePath:String):void {
			var s:Sound = new Sound();
			s.addEventListener(Event.COMPLETE, onSoundLoaded);
			s.addEventListener(IOErrorEvent.IO_ERROR, loadError);
			
			var req:URLRequest = new URLRequest(mp3FilePath);
			s.load(req);
		}
		
		//handle errors loading the mp3
		private function loadError(e:Event = null):void {
			trace('error loading mp3');
		}
		
		private function onSoundLoaded(e:Event):void {
			SparkolBus.tell(MP3_LOADED, '', e);
			if (soundsLoaded < vecFiles.length - 1) {
				soundsLoaded++;
				loadSound(vecFiles[soundsLoaded]);
			}else {
				SparkolBus.tell(ALL_LOADED, '', e);
			}
		}
	}
	
}