package model.data {
	import flash.utils.ByteArray;
	
	/**
	 ** @author Justin O'Neill
	 *  Class to store available skeleton data
	 *  TODO: use ReST services to access skeleton data
	 **/
	
	public class Skeletons {
		
		[Embed(source = 'skeletons.xml', mimeType="application/octet-stream")]
		private static const skeletons:Class;
		private var skeletonsXML:ByteArray;
		public var skeletonsVec:Vector.<Object>;
		
		//load in the xml containing all track parts
		public function Skeletons():void {
			skeletonsXML = new skeletons();
			parseXMLData(new XML(skeletonsXML.toString()));
		}
		
		//parse xml (loaded or embedded) to a skeletonsVec vector of objects and loads skeleton chosen
		private function parseXMLData(xml:XML):void{
			skeletonsVec = new Vector.<Object>;
			var skeletonVO:Object = new Object();
			for(var i: int = 0; i < xml.skeleton.length(); i++){
				skeletonVO = {
					skelefolder:xml.skeleton[i].@skelefolder,
					friendlyName:xml.skeleton[i].@friendlyName,
					slider1:xml.skeleton[i].slider[0].toString(),
					slider2:xml.skeleton[i].slider[1].toString(),
					slider3:xml.skeleton[i].slider[2].toString()
				};
				skeletonsVec.push(skeletonVO);
			}
		}
	}
	
}