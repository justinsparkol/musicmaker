package model {
	import bus.SparkolBus;
	import com.greensock.TweenLite;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.ProgressEvent;
	import flash.events.SampleDataEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.utils.ByteArray;
	import model.Utils;
	import model.SoundManager;
	import model.data.MP3NoSilence;
	import model.data.SoundData;
	CONFIG::saveToMP3 {
	import model.ShineMP3Encoder;
	import com.adobe.audio.format.WAVWriter;
	}
	
	/**
	 ** @author Justin O'Neill
	 *  soundmanager manages the sound registration, stores the data in sounddata objects
	 *  and handles mixing the sound stream to a single bytes 
	 **/
	
	public class SoundManager extends EventDispatcher {
		
		private const BUFFER_SIZE:int = 4096 * 2; //more than enough
		public static const CLIPPING:String = 'clipping';
		
		private var samplesPosition: int = 0;
		private var registeredSounds:Vector.<SoundData>;
		private var activeSounds:Vector.<SoundData>;
		private var output:Sound;
		private var currentOutputBufferPosition:int = 0;
		private var currentLeftOutput:Vector.<Number>;
		private var currentRightOutput:Vector.<Number>;
		private var outputFile:File;
		private var channel:SoundChannel;
		public var trackVolume:Number = 1;
		
		CONFIG::saveToMP3 {
		private var fs:FileStream;
		private var mp3Encoder:ShineMP3Encoder;
		}

		public function SoundManager() {
			if (registeredSounds) {
				for each (var soundData:SoundData in registeredSounds) {
					soundData.leftData = null;
					soundData.rightData = null;
					trace('nulled');
				}
			}
			registeredSounds = new Vector.<SoundData>();
			
			CONFIG::saveToMP3 {
			//initialise mp3 encoder
			mp3Encoder = new ShineMP3Encoder();
			mp3Encoder.addEventListener(Event.COMPLETE, mp3EncodeComplete, false, 0, true);
			mp3Encoder.addEventListener(ProgressEvent.PROGRESS, mp3EncodeProgress, false, 0, true);
			mp3Encoder.addEventListener(ErrorEvent.ERROR, mp3EncodeError, false, 0, true);
			}
		}
		
		//initialise the sound manager and set up the output stream
		public function initSoundManager():void {	
			activeSounds = new Vector.<SoundData>();
			currentLeftOutput = new Vector.<Number>(BUFFER_SIZE);
			currentRightOutput = new Vector.<Number>(BUFFER_SIZE);
			
			output = new Sound();
			output.addEventListener(SampleDataEvent.SAMPLE_DATA, sampleDataHandler, false, 0, true);
		}
		
		//register the sound loaded through sequencer and store in memory (two channels)
		//set up a new sounddata object for the new sound and add it to a vector
		public function registerSound(id:String, sound:Sound, loopLength:Number, volume:Number ):void {
			//trace('register sound', id);
			if (getSoundData(id) != null) {
				return;
			}
			var mp3NoSilence:MP3NoSilence = new MP3NoSilence(sound, loopLength);
			var leftChannel:Vector.<Number> = new Vector.<Number>();
			var rightChannel:Vector.<Number> = new Vector.<Number>();
			var i:int = 0;
			while (mp3NoSilence.mp3Bytes.bytesAvailable) {
				leftChannel[i] = mp3NoSilence.mp3Bytes.readFloat()*volume;
				rightChannel[i] = mp3NoSilence.mp3Bytes.readFloat()*volume;
				i++;
			}
			
			var newSound:SoundData = new SoundData();
			newSound.id = id;
			newSound.leftData = leftChannel.concat();
			newSound.rightData = rightChannel.concat();
			newSound.position = 0;
			newSound.length = leftChannel.length;
			
			registeredSounds.push(newSound);
			
			//mp3NoSilence.mp3Bytes = null;
			//mp3NoSilence = null;
			
		}
		
		//return the data in soundData for the sound id 
		private function getSoundData(id:String):SoundData {
			for each (var soundData:SoundData in registeredSounds) {
				if (soundData.id == id) return soundData;
			}
			return null;
		}
		
		//start streaming the output sound object through the channel soundchannel
		public function beginStream():void {
			
			CONFIG::saveToMP3 {
				outputFile = File.applicationStorageDirectory.resolvePath('Library/Caches/output.dat')
				if (fs == null) {
					fs = new FileStream();
				} else {
					fs.close();
				}
				fs.open(outputFile, FileMode.WRITE);
			}
			
			//if (Utils.randBool()) trackVolume = 0;
			//adjustTrackVolume(1, 5); //fade in over 1 second
			channel = output.play();
		}
		
		//call playsound for the sounds in ids
		public function playSequence(ids:Vector.<String>, completionCallback:Function = null, callbackArgs:Array = null):void {
			if (ids.length == 0) return;
			
			
			ids = ids.concat();
			var initialID:String = ids.shift();
			
			playSound(initialID, playSequenceHelper_(ids, completionCallback, callbackArgs), callbackArgs);
		}
		
		//plays the sound passed in through playsequence and calls the function passed in for when the sound sample events cease
		public function playSound(id:String, completionCallback:Function = null, callbackArgs:Array = null):void {
			if(id!=''){
				trace("In da mix: " + id);
				var soundData:SoundData = getSoundData(id);
				if (!soundData) {
					//trace("SoundManager: Sound id \""+id+"\" has not been registered!");
					if (completionCallback != null) {
						completionCallback.call(this, callbackArgs);
					}
					return;
				}
				
				// copy the sound data object:
				soundData = soundData.dataClone();
				soundData.completionCallback = completionCallback;
				soundData.callbackArgs = callbackArgs;
				soundData.position = -currentOutputBufferPosition;
				activeSounds.push(soundData);
			}
		}
		
		//returns the next ID or calls the complete event to initiate the next loop
		private function playSequenceHelper_(ids:Vector.<String>, finalCallback:Function, callbackArgs:Array = null):Function	{
			if (ids.length == 0) {
				
				return finalCallback;
			}
			var currentID:String = ids.shift();
			return function():void {
				playSound(currentID, playSequenceHelper_(ids, finalCallback, callbackArgs), callbackArgs);
			};
		}
		
		//handle receiving sampledata from the output stream and
		//mix in the active sounds according to their volume
		private function sampleDataHandler(e:SampleDataEvent):void {
			var i:int;
			
			// clear buffer vectors:
			for (i=0; i<BUFFER_SIZE; i++) {
				currentLeftOutput[i] = 0;
				currentRightOutput[i] = 0;
			}
			
			// go over all currently active SoundData objects and add their data to the buffers:
			for (var j:int = 0; j<activeSounds.length; j++) {
				currentOutputBufferPosition = 0;
				
				var currentSound:SoundData = activeSounds[j];
				var leftData:Vector.<Number> = currentSound.leftData;
				var rightData:Vector.<Number> = currentSound.rightData;
				var outputStartIndex:int = 0;
				
				if (currentSound.position < 0) {
					outputStartIndex = -currentSound.position;
					currentSound.position = 0;
				}
				
				var outputEndIndex:int = BUFFER_SIZE;
				var soundEndsDuringThisCycle:Boolean = false;
				var remainingSamples:int = currentSound.length-currentSound.position;
				if (remainingSamples-outputStartIndex <= BUFFER_SIZE) {
					outputEndIndex = remainingSamples;
					soundEndsDuringThisCycle = true;
				}
				
				var dataPos:int;

				dataPos = currentSound.position;
				for (i=outputStartIndex; i<outputEndIndex; i++) {
					currentLeftOutput[i] += leftData[dataPos];
					dataPos++;
				}
				dataPos = currentSound.position;
				for (i=outputStartIndex; i<outputEndIndex; i++) {
					currentRightOutput[i] += rightData[dataPos];
					dataPos++;
				}
				currentSound.position = dataPos;
				
				currentOutputBufferPosition = outputEndIndex;
				if (soundEndsDuringThisCycle) {
					//trace("SoundManager: sound \""+currentSound.id+"\" is ending. Current buffer position: "+currentOutputBufferPosition);
					activeSounds.splice(j, 1);
					j--;
					if (currentSound.completionCallback != null) {
						if (currentSound.callbackArgs == null) {
							currentSound.completionCallback.call(this);
						} else {
							currentSound.completionCallback.call(this, currentSound.callbackArgs);
						}
					}
				}
			}
			
			// copy buffers to sample data ByteArray:
			
				var samples:ByteArray = e.data;
				var valFloat:Number;
				for (i=0; i<BUFFER_SIZE; i++) {
					samples.writeFloat(currentLeftOutput[i] * trackVolume);
					samples.writeFloat(currentRightOutput[i] * trackVolume);
					
					CONFIG::saveToMP3 {
						//write to fs for bytearray to wav to mp3 
						fs.writeFloat(Utils.checkValidFloat(((currentLeftOutput[i] * trackVolume) + (currentRightOutput[i] * trackVolume)) / 2));  //16 bit	
					}
					valFloat = ((currentLeftOutput[i] * trackVolume) + (currentRightOutput[i] * trackVolume)) / 2;
					if (valFloat <= -0.6 || valFloat >= 0.6) {
						SparkolBus.tell(CLIPPING);
					}
				}
			
		}
		
		//tween trackvolume over time
		public function adjustTrackVolume(fadeTo:Number, fadeLength:Number, delayFade:Number = 0):void {
			TweenLite.to(this, fadeLength, { trackVolume:fadeTo, delay:delayFade } );
		}
		
		//stop the stream
		public function endStream():void {
			channel.stop();
		}
		
		CONFIG::saveToMP3 {
			public function exportSongToMP3():void {
				
				trace('exportSongToMP3');
				fs.close();
				var mp3FileStream:FileStream = new FileStream();
				mp3FileStream.open(outputFile, FileMode.READ);
				var wav:ByteArray = new ByteArray();
				var wavWrite:WAVWriter = new WAVWriter();
					wavWrite.numOfChannels = 2;
					wavWrite.sampleBitRate = 16;
					wavWrite.samplingRate = 44100;
					// convert sound data to wav
					
				var soundData:ByteArray = new ByteArray();
				mp3FileStream.position = 0;
				
				while (mp3FileStream.bytesAvailable) {
					mp3FileStream.readBytes(soundData);
				}

				soundData.position = 0;
				wavWrite.processSamples(wav, soundData, 44100, 1);
				wav.position = 0;
				// encode
				mp3Encoder.start(wav);
				
			}

			private function mp3EncodeProgress(event : ProgressEvent) : void {
				trace('mp3EncodeProgress' + ((event.bytesLoaded / event.bytesTotal)*100));
			} 

			private function mp3EncodeError(event : ErrorEvent) : void {
				trace("Error : ", event.text);
			}
				
			private function mp3EncodeComplete(event : Event) : void {	
				mp3Encoder.saveAs('untitled.mp3');
				mp3Encoder = null;
				trace('done');
			}
		}
		
	}
}


/*
	Copyright (c) 2012 Philipp Seifried
	
	Permission is hereby granted, free of charge, to any person obtaining 
	a copy of this software and associated documentation files (the "Software"), 
	to deal in the Software without restriction, including without limitation 
	the rights to use, copy, modify, merge, publish, distribute, sublicense, 
	and/or sell copies of the Software, and to permit persons to whom the 
	Software is furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included 
	in all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
	THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
	THE SOFTWARE.
	*/